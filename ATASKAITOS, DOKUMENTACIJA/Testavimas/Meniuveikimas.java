// Generated by Selenium IDE
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
public class Meniuveikimas {
  private WebDriver driver;
  private Map<String, Object> vars;
  JavascriptExecutor js;
  @Before
  public void setUp() {
    driver = new FirefoxDriver();
    js = (JavascriptExecutor) driver;
    vars = new HashMap<String, Object>();
  }
  @After
  public void tearDown() {
    driver.quit();
  }
  @Test
  public void meniuveikimas() {
    driver.get("http://lukkaz3.stud.if.ktu.lt/ktu/index.php");
    driver.manage().window().setSize(new Dimension(1382, 744));
    driver.findElement(By.cssSelector("#menu > ul > li:nth-child(2) > a")).click();
    driver.findElement(By.name("home")).click();
    driver.findElement(By.cssSelector("#menu > ul > li:nth-child(3) > a")).click();
    driver.findElement(By.id("selected")).click();
    driver.findElement(By.cssSelector("li:nth-child(4) li:nth-child(1) > a")).click();
    driver.findElement(By.id("selected")).click();
    driver.findElement(By.cssSelector("li:nth-child(4) li:nth-child(2) > a")).click();
    driver.findElement(By.cssSelector("li:nth-child(4) li:nth-child(3) > a")).click();
    driver.findElement(By.cssSelector("li:nth-child(4) li:nth-child(3) > a")).click();
    driver.findElement(By.cssSelector("li:nth-child(4) li:nth-child(4) > a")).click();
    driver.findElement(By.cssSelector("li:nth-child(4) li:nth-child(5) > a")).click();
    driver.findElement(By.cssSelector("li:nth-child(4) li:nth-child(6) > a")).click();
    driver.findElement(By.cssSelector("li:nth-child(4) li:nth-child(7) > a")).click();
    driver.findElement(By.cssSelector("li:nth-child(4) li:nth-child(8) > a")).click();
    driver.findElement(By.cssSelector("li:nth-child(4) li:nth-child(9) > a")).click();
    driver.findElement(By.id("selected")).click();
    driver.findElement(By.linkText("VFSA")).click();
    driver.findElement(By.cssSelector(".zetas > li:nth-child(1) > a")).click();
    driver.findElement(By.linkText("ESA")).click();
    driver.findElement(By.cssSelector(".zetas > li:nth-child(4) > a")).click();
    driver.findElement(By.cssSelector(".zetas > li:nth-child(4) > a")).click();
    driver.findElement(By.linkText("FUMSA")).click();
    driver.findElement(By.cssSelector(".zetas > li:nth-child(6) > a")).click();
    driver.findElement(By.cssSelector(".zetas > li:nth-child(7) > a")).click();
    driver.findElement(By.linkText("SHM")).click();
    driver.findElement(By.linkText("STATIUS")).click();
    driver.findElement(By.id("selected")).click();
  }
}
