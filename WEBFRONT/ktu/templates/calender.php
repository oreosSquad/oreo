<!DOCTYPE html>
<html>
    <head>
        <title>kalendorius</title>
       
        <meta charset="utf-8">
        <link rel="stylesheet" href="style/cal_style.css">

    </head>
    <body>
        
        <?php
        
        include 'style/config.php';
        include 'style/mysql.php';
        
        
        $query = "SELECT pavadinimas, DAY(data) AS day, ID FROM renginys WHERE MONTH(data) = $m AND YEAR(data) = $y";
        
        $result = mysql::query($query);
    
        ?>

  <main>
    <div class="toolbar">
     
      <div class="date_shown"><?php echo $y ."-". $m ;?></div>
      <!--class="current-month"-->
      
    </div>
    <div class="calendar">
      <div class="calendar__header">
        <div>mon</div>
        <div>tue</div>
        <div>wed</div>
        <div>thu</div>
        <div>fri</div>
        <div>sat</div>
        <div>sun</div>
      </div>
      <div class="calendar__week">
          <?php
          $i = '';
          for($i = 1; $i <= 7 ; $i++){
              echo "<div class="."calendar__day day".">$i </br>";
              
            while($row = $result->fetch_assoc())
            {
              if($row["day"] == $i){
                echo "<a href="."event_info.php?id={$row["ID"]}".">{$row["pavadinimas"]}</a></br>";  
              }
            } 
            echo "</div>";
            $result->data_seek(0);
          }
          ?>
        
        
      </div>
      <div class="calendar__week">
          <?php
          $i = '';
          for($i = 8; $i <= 14 ; $i++){
              echo "<div class="."calendar__day day".">$i </br>";
              
            while($row = $result->fetch_assoc())
            {
              if($row["day"] == $i){
                echo "<a href="."event_info.php?id={$row["ID"]}".">{$row["pavadinimas"]}</a></br>";  
              }
            } 
            echo "</div>";
            $result->data_seek(0);
          }
          ?>     
      </div>
      <div class="calendar__week">
        <?php
          $i = '';
          for($i = 15; $i <= 21 ; $i++){
              echo "<div class="."calendar__day day".">$i </br>";
              
            while($row = $result->fetch_assoc())
            {
              if($row["day"] == $i){
                echo "<a href="."event_info.php?id={$row["ID"]}".">{$row["pavadinimas"]}</a></br>";  
              }
            } 
            echo "</div>";
            $result->data_seek(0);
          }
          ?>  
      </div>
      <div class="calendar__week">
        <?php
          $i = '';
          for($i = 22; $i <= 28 ; $i++){
              echo "<div class="."calendar__day day".">$i </br>";
              
            while($row = $result->fetch_assoc())
            {
              if($row["day"] == $i){
                echo "<a href="."event_info.php?id={$row["ID"]}".">{$row["pavadinimas"]}</a></br>";  
              }
            } 
            echo "</div>";
            $result->data_seek(0);
          }
          ?>
      </div>
      <div class="calendar__week">
        <?php
          $i = '';
          for($i = 29; $i <= 31 ; $i++){
              echo "<div class="."calendar__day day".">$i </br>";
              
            while($row = $result->fetch_assoc())
            {
              if($row["day"] == $i){
                echo "<a href="."event_info.php?id={$row["ID"]}".">{$row["pavadinimas"]}</a></br>";  
              }
            } 
            echo "</div>";
            $result->data_seek(0);
          }
          ?>
        <div class="calendar__day day">1</div>
        <div class="calendar__day day">2</div>
        <div class="calendar__day day">3</div>
        <div class="calendar__day day">4</div>
      </div>
    </div>
  </main>

    </body>
</html>