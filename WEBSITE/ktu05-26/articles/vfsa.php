<?php
session_start();
?>
﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

<head>
  <title>KTU events</title>
  <meta http-equiv="content-type" content="text/html; utf-8" />
  
  <!-- **** layout stylesheet **** -->
  <link rel="stylesheet" type="text/css" href="../style/style.css" /> 

  <!-- **** colour scheme stylesheet **** -->
  <link rel="stylesheet" type="text/css" href="../style/colour.css" />
  
  <link rel="stylesheet" type="text/css" href="../style/stylemenu.css"/>

</head>

<body>
  <div id="main">

   <div id="logo"><h1></h1></div>
   
    <div id="content">
      <div id="menu">
        <ul>
          <li><a href="index.html">Pagrindinis</a></li>
          <li><a href="templates/calendar.php">Kalendorius</a></li>
          <li><a href="../articles/ktu.php">Universitetas</a></li>
       
      <li><a href="#">Fakultetai</a>
             <ul>
                        <li><a href="../articles/chemija.php">Cheminės technologijos fakultetas</a></li>
                        <li><a href="../articles/ekonom.php">Ekonomikos ir verslo fakultetas</a></li>
                        <li><a href="../articles/elektra.php">Elektros ir elektronikos fakultetas</a></li>
                        <li><a href="../articles/informatika.php">Informatikos fakultetas</a></li>
                        <li><a href="../articles/matematika.php">Matematikos ir gamtos mokslų fakultetas</a></li>
                        <li><a href="../articles/mechanika.php">Mechanikos inžinerijos ir dizaino fakultetas</a></li>
                        <li><a href="../articles/panevez.php">Panevėžio technologijų ir verslo fakultetas</a></li>
                        <li><a href="../articles/soc.php">Socialinių, humanitarinių mokslų ir menų fakultetas</a></li>
                        <li><a href="../articles/statybos.php">Statybos ir architektūros fakultetas</a></li>
                            
                    </ul>
           </li>


          
      <li><a href="#">Studentų atstovybės</a>
             <ul class="zetas">
              
                        <li><a href="../articles/vivat.php">VIVAT chemija</a></li>
                        <li><a href="../articles/vfsa.php">VFSA</a></li>
                        <li><a href="../articles/esa.php">ESA</a></li>
                        <li><a href="../articles/infosa.php">InfoSA</a></li>
                        <li><a href="../articles/fumsa.php">FUMSA</a></li>
                        <li><a href="../articles/indi.php">InDi</a></li>
                        <li><a href="../articles/pafisa.php">PanFiSA</a></li>
                        <li><a href="../articles/shm.php">SHM</a></li>
                        <li><a href="../articles/statius.php">STATIUS</a></li>
                            
                    </ul>
           </li>
          <li><a href="contact.html">Apie</a></li>
        </ul>
      </div>
      <div id="column1">
        
        <div class="sidebaritem">
          <div class="sbihead">
            <h1>Prisijungimas ir registracija</h1>
          </div>
          <div class="sbilinks">
            <!-- **** INSERT ADDITIONAL LINKS HERE **** -->
            <ul>
                <li><a <?php echo isset($_SESSION['u_name'])? 'class="isDisabled"' : ''; ?> href="templates/login_form.php">prisijungimas</a></li>
                <li><a href="templates/signup_form_u.php">registracija naudotojui</a></li>
                <li><a href="templates/signup_form_o.php">registracija organizatoriui</a></li>
             <li><a href="control/logout.php">atsijungti</a></li>
            </ul>
            
          </div>
        </div>
          <div class="sidebaritem">
          <div class="sbihead">
            <h1>renginiu tvarkymas</h1>
          </div>
          <div class="sbicontent">
            <!-- **** INSERT NEWS ITEMS HERE **** -->
            <ul>
                <li><a href="templates/event_form.php">naujas renginys</a></li>
                <li><a href="#">mano renginai</a></li>
             
            </ul>
          </div>
        </div>
        <div class="sidebaritem">
          <div class="sbihead">
            <h1>Kita informacija</h1>
          </div>
          <div class="sbicontent">
            <!-- **** INSERT OTHER INFORMATION HERE **** -->
            <p>
             Tai IFA-7/1 studentų projektinis darbas
            </p>
          </div>
        </div>
      </div>
      <div id="column2">
        <h1>VFSA</h1>
        <!-- **** INSERT PAGE CONTENT HERE **** -->
        <p>
            <img src="../style/SA/juodas (1).png" alt="VFSA" width="282" height="329"/>
        </p>
        <p>
     VFSA (anksčiau „V.F.S.A.“) ištakos siekia 1968 metus, kai buvo įkurta Kauno Antano Sniečkaus politechnikos instituto inžinerinės ekonomikos fakulteto Studentų profsąjunga. Bet mes nesame tokie seni ir skaičiuojame tik dabar suėjusius 24-uosius metus (nuo 1993 m. įkūrimo).
        </p>
        <p>
    Tai studentų organizacija, skirta atstovauti KTU Ekonomikos ir verslo fakulteto studentų poreikius bei spręsti su studijomis susijusius klausimus.  
        </p>
        
           <p>
     
        <a href="https://www.facebook.com/fsavfsa/">VFSA Facebook</a>
         </p>
          <p>
        <a href="https://sa.ktu.edu/ekonomikos-ir-verslo-fsa/">VFSA KTU puslapis</a>
        </p>
        
     
       
  </div>
       
</body>
</html>


