<?php
session_start();
?>
﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

<head>
  <title>KTU events</title>
  <meta http-equiv="content-type" content="text/html; utf-8" />
  
 
<meta name="viewport" content="width=320, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; minimum-scale=1.0; user-scalable=0;"/>
<meta name="viewport" content="width=device-width, target-densityDpi=medium-dpi"/>


  <!-- **** layout stylesheet **** -->
  <link rel="stylesheet" type="text/css" href="style/style.css" /> 

  <!-- **** colour scheme stylesheet **** -->
  <link rel="stylesheet" type="text/css" href="style/colour.css" />
  
  <link rel="stylesheet" type="text/css" href="style/stylemenu.css"/>

</head>

<body>
  <div id="main">
   <!-- <div id="links">
      <!-- **** INSERT LINKS HERE **** 
      <a href="#">another link</a> | <a href="#">another link</a> | <a href="#">another link</a> | <a href="#">another link</a>
    </div>-->
<!--      <div>
    <img src="style/redyeloo.jpg" alt="logo" style="width:150px;height:150px; " align="top"/>
      </div>-->
   <div id="logo"><h1></h1></div>
   <style>
        body{
            background-image: url("style/ktuif.jpg");
            background-repeat: no-repeat;
            background-size: cover;
        }
        
        </style>
     <!-- <div id="redyeloo"><h1>KTU events</h1></div>-->
    <div id="content">
      <div id="menu">
        <ul>
          <li><a id="selected" href="index.php">Pagrindinis</a></li>
          <li><a href="templates/calendar.php">Kalendorius</a></li>
          <li><a href="index.php?article=19">Universitetas</a></li>
       
      <li><a href="#">Fakultetai</a>
             <ul>
                 <li><a href="index.php?article=1">Cheminės technologijos fakultetas</a></li>
                        <li><a href="index.php?article=2">Ekonomikos ir verslo fakultetas</a></li>
                        <li><a href="index.php?article=3">Elektros ir elektronikos fakultetas</a></li>
                        <li><a href="index.php?article=4">Informatikos fakultetas</a></li>
                        <li><a href="index.php?article=5">Matematikos ir gamtos mokslų fakultetas</a></li>
                        <li><a href="index.php?article=6">Mechanikos inžinerijos ir dizaino fakultetas</a></li>
                        <li><a href="index.php?article=7">Panevėžio technologijų ir verslo fakultetas</a></li>
                        <li><a href="index.php?article=8">Socialinių, humanitarinių mokslų ir menų fakultetas</a></li>
                        <li><a href="index.php?article=9">Statybos ir architektūros fakultetas</a></li>
                            
                    </ul>
           </li>


          
      <li><a href="#">Studentų atstovybės</a>
             <ul class="zetas">
              
                        <li><a href="index.php?article=10">VIVAT chemija</a></li>
                        <li><a href="index.php?article=11">VFSA</a></li>
                        <li><a href="index.php?article=12">ESA</a></li>
                        <li><a href="index.php?article=13">InfoSA</a></li>
                        <li><a href="index.php?article=14">FUMSA</a></li>
                        <li><a href="index.php?article=15">InDi</a></li>
                        <li><a href="index.php?article=16">PanFiSA</a></li>
                        <li><a href="index.php?article=17">SHM</a></li>
                        <li><a href="index.php?article=18">STATIUS</a></li>
                            
                    </ul>
           </li>
         
        </ul>
      </div>
      <div id="column1">
        
        <div class="sidebaritem">
          <div class="sbihead">
            <h1>Prisijungimas ir registracija</h1>
          </div>
          <div class="sbilinks">
            <!-- **** INSERT ADDITIONAL LINKS HERE **** -->
            <ul>
                <li><a <?php echo isset($_SESSION['u_name'])? 'class="isDisabled"' : ''; ?> href="templates/login_form.php">prisijungimas</a></li>
                <li><a href="templates/signup_form_u.php">registracija naudotojui</a></li>
                <li><a href="templates/signup_form_o.php">registracija organizatoriui</a></li>
             <li><a href="control/logout.php">atsijungti</a></li>
            </ul>
            
          </div>
        </div>
          <div class="sidebaritem">
          
             <?php
           if(isset($_SESSION['type'])){
               if( $_SESSION['type'] == 'org'){
             echo  "
                     <div class='sbihead'>
                     <h1>renginiu tvarkymas</h1>
                    </div>
                     <div class='sbicontent'>
            
            <ul>
                <li><a href='templates/event_form.php'>naujas renginys</a></li>
                <li><a href='templates/events_list.php'>mano renginai</a></li>
             
            </ul>
               </div>";}
           }
           
           
           ?>
        </div>
        <div class="sidebaritem">
          <div class="sbihead">
            <h1>Kita informacija</h1>
          </div>
          <div class="sbicontent">
            <!-- **** INSERT OTHER INFORMATION HERE **** -->
            <p>
             Tai IFA-7/1 studentų projektinis darbas
            </p>
          </div>
        </div>
      </div>
      <div id="column2">
     <?php
     if(isset($_GET["article"])){
         $num = $_GET["article"];
         switch($num){
             case"1":
                 include 'articles/chemija.php';
                 break;
                case"2":
                 include 'articles/ekonom.php';
                    break;
                    case"3":
                 include 'articles/elektra.php';
                        break;
                        case"4":
                 include 'articles/informatika.php';
                            break;
                            case"5":
                 include 'articles/matematika.php';
                                break;
                                case"6":
                 include 'articles/mechanika.php';
                                    break;
                                    case"7":
                 include 'articles/panevez.php';
                                        break;
                                        case"8":
                 include 'articles/soc.php';
                                            break;
                                        case"9":
                 include 'articles/statybos.php';
                                            break;
                                            case"10":
                 include 'articles/vivat.php';
                                                break;
                                                case"11":
                 include 'articles/vfsa.php';
                                                    break;
                                                    case"12":
                 include 'articles/esa.php';
                                                        break;
                                                        case"13":
                 include 'articles/infosa.php';
                                                            break;
                                                            case"14":
                 include 'articles/fumsa.php';
                                                                break;
                                                                case"15":
                 include 'articles/indi.php';
                                                                    break;
                                                                    case"16":
                 include 'articles/pafisa.php';
                                                                        break;
                                                                        case"17":
                 include 'articles/shm.php';
                                                                            break;
                                                                            case"18":
                 include 'articles/statius.php';
                                                                                break;
                                                                            case"19":
                 include 'articles/ktu.php';
                                                                            
                 
         }
                 }
                 else 
                 {
                     include 'templates/upcoming_events.php';
                     
                 }
     ?>
        
     
       
  </div>
       
</body>
</html>