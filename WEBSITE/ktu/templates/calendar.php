<?php
session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>kalendorius</title>
       
        <meta charset="utf-8">
        <link rel="stylesheet" href="../style/cal_style.css">
    
    </head>
    <body>
        
        <?php
        
        
        $_SESSION['capnum'] = ((isset($_SESSION['capnum'])) ? $_SESSION['capnum'] : 0);

       ?>
        
        <?php
        
        include '../utils/config.php';
        include '../utils/mysql.php';
        
        
        
    
        ?>
        <div>
            
            <form action="#" method="POST">
            <input type="submit" name="back" value="atgal" />
            <input type="submit" name="next" value="pirmyn" />
            <input type="submit" name="home" value="grįžti į meniu" />
            </form>
        </div>
        
        <?php
      
//echo $_SESSION['capnum'];

                 if(isset($_POST['next'])){
                     $_SESSION['capnum']++;
                     header("Location: calendar.php");
                 }
                 
                 if(isset($_POST['back'])){
                     $_SESSION['capnum']--;
                     header("Location: calendar.php"); 
                 }
                 
                 if(isset($_POST['home'])){
                     $_SESSION['capnum'] = 0;
                     header("Location: ../index.php"); 
                 }
                     
              
                  $now = date("m");
                  $date = date('Y-m-d', mktime(0,0,0,$now+$_SESSION['capnum'],1));  // esamo menesio data + offset
                  $weekday = date("l", mktime(0,0,0,$now+$_SESSION['capnum'],1)); //kuria savaites diena menesis prasideda
                  $daycount = date("t", mktime(0,0,0,$now+$_SESSION['capnum'],1)); //kiek dienu menesyje
                  $pieces = explode("-", $date);
                  $y = intval($pieces[0]);
                  $m = intval($pieces[1]); 
                  
                  $query = "SELECT pavadinimas, DAY(data) AS day, ID FROM renginys WHERE MONTH(data) = $m AND YEAR(data) = $y";
        
                  $result = mysql::query($query);
        ?>
  <main>
    <div class="toolbar">
     
      <div class="date_shown"><?php echo $y ."-". $m ;?></div>
      <!--class="current-month"-->
      
    </div>
    <div class="calendar">
      <div class="calendar__header">
        <div>mon</div>
        <div>tue</div>
        <div>wed</div>
        <div>thu</div>
        <div>fri</div>
        <div>sat</div>
        <div>sun</div>
      </div>
      <div class="calendar__week">
          <?php
          
          $f = ''; //offset
          
         switch ($weekday) {
        case "Monday":
        $f = 0;
        break;
        case "Tuesday":
        $f = -1;
        break;
        case "Wednesday":
        $f = -2;
        break;
        case "Thursday":
        $f = -3;
        break;
        case "Friday":
        $f = -4;
        break;
        case "Saturday":
        $f = -5;
        break;
        case "Sunday":
        $f = -6;
        break;
         default:
        $f = 0;
                    }
          
          $i = '';
          for($i = 1 + $f; $i <= 7 + $f; $i++){
              
              if($i <= 0)
              {
                   echo "<div class="."calendar__day day"."></br>";
              }else
                  {
                    echo "<div class="."calendar__day day".">$i </br>";
                  }
            while($row = $result->fetch_assoc())
            {
              if($row["day"] == $i){
                echo "<a href="."event_info.php?id={$row["ID"]}".">{$row["pavadinimas"]}</a></br>";  
              }
            } 
            echo "</div>";
            $result->data_seek(0);
          }
          ?>
        
        
      </div>
      <div class="calendar__week">
          <?php
          $i = '';
          for($i = 8+ $f; $i <= 14 + $f; $i++){
              echo "<div class="."calendar__day day".">$i </br>";
              
            while($row = $result->fetch_assoc())
            {
              if($row["day"] == $i){
                echo "<a href="."event_info.php?id={$row["ID"]}".">{$row["pavadinimas"]}</a></br>";  
              }
            } 
            echo "</div>";
            $result->data_seek(0);
          }
          ?>     
      </div>
      <div class="calendar__week">
        <?php
          $i = '';
          for($i = 15+ $f; $i <= 21 + $f; $i++){
              echo "<div class="."calendar__day day".">$i </br>";
              
            while($row = $result->fetch_assoc())
            {
              if($row["day"] == $i){
                echo "<a href="."event_info.php?id={$row["ID"]}".">{$row["pavadinimas"]}</a></br>";  
              }
            } 
            echo "</div>";
            $result->data_seek(0);
          }
          ?>  
      </div>
      <div class="calendar__week">
        <?php
          $i = '';
          for($i = 22+ $f; $i <= 28+ $f ; $i++){
              echo "<div class="."calendar__day day".">$i </br>";
              
            while($row = $result->fetch_assoc())
            {
              if($row["day"] == $i){
                echo "<a href="."event_info.php?id={$row["ID"]}".">{$row["pavadinimas"]}</a></br>";  
              }
            } 
            echo "</div>";
            $result->data_seek(0);
          }
          ?>
      </div>
      <div class="calendar__week">
        <?php
          $i = '';
          for($i = 29+ $f; $i <= 35+ $f ; $i++){
              if($i > $daycount){
                echo "<div class="."calendar__day day"."></br>";  
              }
              else{
               echo "<div class="."calendar__day day".">$i </br>";   
              }
              
              
            while($row = $result->fetch_assoc())
            {
              if($row["day"] == $i){
                echo "<a href="."event_info.php?id={$row["ID"]}".">{$row["pavadinimas"]}</a></br>";  
              }
            } 
            echo "</div>";
            $result->data_seek(0);
          }
          ?>
<!--        <div class="calendar__day day">1</div>
        <div class="calendar__day day">2</div>
        <div class="calendar__day day">3</div>
        <div class="calendar__day day">4</div>-->
      </div>
        <?php
        if(($weekday == "Sunday" && $daycount > 29) || ($weekday == "Saturday" && $daycount > 30))
        {
           
        echo "<div class="."calendar__week".">";
        
            $i = '';
             for($i = 36+ $f; $i <= 42+ $f ; $i++){
                if($i > $daycount)
                {
                    echo "<div class="."calendar__day day"."></br>";  
                }
                else{
                    echo "<div class="."calendar__day day".">$i </br>";   
                    }
              
            while($row = $result->fetch_assoc())
            {
                 if($row["day"] == $i){
                    echo "<a href="."event_info.php?id={$row["ID"]}".">{$row["pavadinimas"]}</a></br>";  
                 }
            } 
            echo "</div>";
            $result->data_seek(0);
          }
          
     
        echo "</div>"; 
            
        }
        
        ?>
    </div>
  </main>

    </body>
</html>