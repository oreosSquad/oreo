<?php
  session_start();
   include '../utils/config.php';
   include '../utils/mysql.php';

 $query = "SELECT * FROM renginys"
         . " WHERE fk_organizatoriusID = '{$_SESSION['id']}' ";
 $data = mysql::query($query);
 
?>
<!DOCTYPE html>
<html>
    <head>
        <title>renginiai</title>
        <style>
            table {
	font-family: arial, sans-serif;
	border-collapse: collapse;
	width: 850px;
        
}

td, th {
	border: 1px solid #dddddd;
	text-align: left;
        font: bold;
	padding: 12px 15px;
        
}

thead tr {
    background-color: #ff8000;
    color: white;
}

tbody tr:nth-of-type(even){
    background-color: lightgray;
}

tbody tr:nth-of-type(odd){
    background-color: white;
}

tbody tr:last-of-type{
    border-bottom: 2px solid #333;
}

#tablebox{
    width: 850px;
    height: auto;
    padding-top: 100px;
    margin-left: auto;
    margin-right: auto;
}

body{
    background-color: #404040;
}
        </style> 
    </head>
    <body>
        <div id="tablebox">
<?php  
       if($data->num_rows > 0 ){
       echo "<table>
            <thead>
            <tr>
                <th>Pavadinimas</th>
                <th>data</th>
                <th>laikas</th>
                <th>vieta</th>
                <th>fakultetas</th>
                <th>atstovybe</th>
                <th>tipas</th>
                <th>parinktys</th>
            </tr>
            </thead>
            <tbody>";
            while($row = $data->fetch_assoc())
                    {
                    echo "<tr>
                        <td> {$row['pavadinimas']}  </td>
                        <td> {$row['data']}  </td>
                        <td> {$row['laikas']}  </td>
                        <td> {$row['vieta']}  </td>
                        <td> {$row['fk_fakultetas']}  </td>
                        <td> {$row['fk_atstovybe']} </td>
                        <td> {$row['fk_tipas']}  </td>
                            <td>
                                <a href='event_edit_form.php?id={$row["ID"]}'>redaguoti</a>
                                <a href='../control/event_del.php?id={$row["ID"]}'>naikinti</a>
                            </td>
                        </tr>";
                    }
        echo  "</tbody>
               </table>";
       }else{
           echo "0 rezultatu";
       }
?>
        </div>
    </body>
</html>


