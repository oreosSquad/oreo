<!DOCTYPE html>
<?php

include  '../utils/config.php';
include '../utils/mysql.php';

$id = $_GET['id'];

$query = "SELECT * "
        . "FROM renginys "
        . "WHERE ID = $id";

$data = mysql::query($query);

$row = mysqli_fetch_assoc($data);

$query2 = "SELECT pavadinimas"
        . " FROM organizatorius"
        . " WHERE ID = {$row["fk_organizatoriusID"]}";
        
$data2 = mysql::query($query2);
        
$orgpav = mysqli_fetch_assoc($data2);
?>
<html>
    <head>
        <title>renginio informacija</title>
        <link rel="stylesheet" type="text/css" href="../style/event_info.css" />
        <link rel="stylesheet" type="text/css" href="../style/800px.css" media="screen and (max-width: 800px)" />
    </head>
    <body>
        <div id="container">
            
            <div id="imagebox">
                <img src="../style/redyeloo.JPG" alt="logo" width="300" height="300">
            </div>
            <div id="mainbody">
                <div id="atribute">
                    <div id="title">pavadinimas</div>
                    <div id="infotext"><?php echo $row["pavadinimas"] ?></div>
                </div>
                <div id="atribute">
                     <div id="title">data</div>
                    <div id="infotext"><?php echo $row["data"] ?></div>
                </div>
                <div id="atribute">
                     <div id="title">vieta</div>
                    <div id="infotext"><?php echo $row["vieta"] ?></div>
                </div>
                <div id="atribute">
                     <div id="title">laikas</div>
                    <div id="infotext"><?php echo $row["laikas"] ?></div>
                </div>
                <div id="atribute">
                     <div id="title">tipas</div>
                    <div id="infotext"><?php echo $row["fk_tipas"] ?></div>
                </div>
                <div id="atribute">
                     <div id="title">atstovybe</div>
                    <div id="infotext"><?php echo $row["fk_atstovybe"] ?></div>
                </div>
                <div id="atribute">
                     <div id="title">fakultetas</div>
                    <div id="infotext"><?php echo $row["fk_fakultetas"] ?></div>
                </div>
                <div id="atribute">
                     <div id="title">organizatorius</div>
                    <div id="infotext"><?php echo $orgpav["pavadinimas"] ?></div>
                </div>
            </div>
            <div id="footer">
                <textarea rows="30" cols="100" >
                    <?php echo $row["aprasymas"] ?>
                </textarea>
            </div>
        </div>
    </body>
</html>

